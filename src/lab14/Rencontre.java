package lab14;

import java.util.*;
import lab14.Invitation;

public class Rencontre {

     Scanner clavier;
	public static void main(String[] args) {

		Invitation invite1 = new Invitation("Marie");
		Invitation invite2 = new Invitation("Paul");
		Invitation invite3 = new Invitation("Jean");

		System.out.println("Invitation numéro "+invite1.getNumero() + " Pour "+invite1.getNom());
		System.out.println("Invitation numéro "+invite2.getNumero() + " Pour "+invite2.getNom());
		System.out.println("Invitation numéro "+invite3.getNumero() + " Pour "+invite3.getNom());
		System.out.println("Changement numéros invitations");
		invite1.setNumero(3);
		invite3.setNumero(1);
		invite2.setNumero(15);
		System.out.println("Invitation numéro "+invite1.getNumero() + " Pour "+invite1.getNom());
		System.out.println("Invitation numéro "+invite2.getNumero() + " Pour "+invite2.getNom());
		System.out.println("Invitation numéro "+invite3.getNumero() + " Pour "+invite3.getNom());
	}
	

    
}
